// Register input text and button events.
document.getElementById("generateButton").addEventListener("click", generateAction);
document.getElementById("apiKey").addEventListener("change", saveApiKey);

// Initialize the form with the user's option settings
chrome.storage.local.get("apiKeyValue", function(items){
    document.getElementById("apiKey").value = items['apiKeyValue'];
});

// Immediately persist options changes
function saveApiKey(event) {
    var value = event.target.value;
    chrome.storage.local.set({apiKeyValue: value});
}

// Parse Lodestone pages with Triple Triad Cards
function generateAction(){
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        var pageCount = 4;
        var currTab = tabs[0];
        var patt = /^.*\/lodestone\/character\/([0-9]+)\/goldsaucer\/tripletriad.*$/
        var apiKey = document.getElementById("apiKey").value;
        var href;
        if (currTab) {
            var match = patt.exec(currTab.url)
            if (match == null) {
                console.log('Wrong URL for Lodestone character: ' + currTab.url);
                return;
            }
            var charId = match[1];
            // Parse all pages by fetching url and saving results to cache.
            for (let page = 1; page <= pageCount; page++) {
                href = new URL(currTab.url);
                href.searchParams.set('page', page.toString());
                href.searchParams.set('hold', '');
                console.log(href.toString());
                fetch(href.toString())
                    .then(
                        function(response) {
                            if (response.status !== 200) {
                                console.log('Looks like there was a problem. Status Code: ' + response.status);
                                return;
                            }
                            // Examine the text in the response
                            response.text().then(function(htmlData) {
                                var cards = getOwnedCards(htmlData);
                                console.log(cards);
                                var ul = document.getElementById("fetchCache");
                                var li = document.createElement("li");
                                li.appendChild(document.createTextNode(cards));
                                ul.appendChild(li);
                                // Merge all results and present results.
                                // This is needed due to fetching uses callbacks.
                                if (ul.childElementCount == pageCount) {
                                    mergeCacheAndPresent(apiKey, charId, ul);
                                    removeAllChildNodes(ul);
                                }
                            });
                        }
                    )
                    .catch(function(err) {
                        console.log('Fetch Error', err);
                    });
            }
        }
    });
}

// Return JSON list of owned card numbers in given html data.
function getOwnedCards(htmlData) {
    var root = document.createElement('html');
    root.innerHTML = htmlData;
    var table = root.getElementsByClassName("tripletriad-card_list");
    var data = [];
    var cardInfo;
    if (table.length != 1) {
        return '';
    }
    var rows = table[0].getElementsByTagName("li");
    var spans = [];
    var nums = [];
    var names = [];
    var anchors = [];
    var dbUrl = '';
    for (let i = 0; i < rows.length; i++) {
        // Unsettled means unowned.
        if (rows[i].className != "unsettled") {
            cardInfo = {number: null, name: null, language: null};
            // Detect number and language.
            spans = rows[i].getElementsByTagName("span");
            if (spans.length > 0) {
                nums = spans[0].getElementsByTagName("span");
                if (nums.length == 1) {
                    cardInfo["number"] = parseInt(nums[0].innerHTML.replace(/\D/g,''), 10);
                }
                cardInfo["language"] = spans[0].className.substring(4);
            }
            // Detect name.
            names = rows[i].getElementsByClassName("name_inner");
            if (names.length > 0) {
                cardInfo["name"] = names[0].innerHTML;
            }
            // Detect database hash.
            anchors = rows[i].getElementsByTagName("a");
            if (anchors.length > 0) {
                dbUrl = anchors[0].href.replace(/\/$/, '');
                cardInfo["database"] = dbUrl.substring(dbUrl.lastIndexOf('/') + 1);
            }
            data.push(cardInfo);
        }
    }
    return JSON.stringify(data);
}

// Merge all lists in cache, sort them numerically and return complete JSON data.
function mergeCacheAndPresent(apiKey, charId, ul) {
    var parts = [];
    var items = ul.getElementsByTagName("li");
    var result = [];
    for (let i = 0; i < items.length; i++) {
        result = JSON.parse(items[i].innerHTML)
        parts.push(result);
    }
    var mergedCards = [].concat.apply([], parts);
    // Sort cards by numbers
    mergedCards.sort(function (a, b) {
        return a.number - b.number;
    });
    document.getElementById("jsonData").innerHTML =  JSON.stringify({
        lalachievementsApiKey: apiKey,
        lodestoneCharacterID: charId,
        tripleTriadOwnedCards: mergedCards
    }, null, 2);
}

// Fancy method instead of innerHTML = ''.
function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

