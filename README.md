# Lodestone Export Chrome Extension #

This is very simple Chrome extension for exporting data from Final Fantasy XIV Online Lodestone character page.

At the moment it can parse Triple Triad page. The result is JSON file which is supposed to be send to external API, e.g https://lalachievements.com/en/api

```json
{
  "lalachievementsApiKey": "<redacted>",
  "lodestoneCharacterID": "<redacted>",
  "tripleTriadOwnedCards": [
    {
      "number": 1,
      "name": "Dodo",
      "language": "eu",
      "database": "a94b2f1604d"
    },
    {
      "number": 3,
      "name": "Sabotender",
      "language": "eu",
      "database": "143d908fe03"
    }
  ]
}
```

## Installation and Usage ##

In Chrome broswer click on Manage Extension, enable Developer Mode, click on Load unpacked and select *src* directory.

For update click on Update and then click on small Reload arrow.

Login to your Lodestone profile, select character, navigate to Triple Triad page, open the extension and click on a button.

## Changelog ##

### 0.1 ###

* Initial working prototype

## Licence ##

MIT

## Contact ##

Michal Siska <michal.515k4@gmail.com>
